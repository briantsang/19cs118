# 19CS118

This project contain three part,frontend,backend and IoT device.

For Backend:
1. download xampp
2. start "Apache" and "MySQL"
3. copy backend/User_Project to C:\xampp\htdocs

For Frontend:
1. open cmd
2. cd to the path of frontend/client
3. type 'npm start'
4. change the ip address to your ip address(Backend) at 
    client/src/Login.js #21
    client/src/ManageBike.js #146,#213
    client/src/RegisterBike.js #97
    client/src/RentBike.js #67
    client/src/SignUp.js #22

For IoT:
1. download Arduino IDE
2. open Iot at Arduino IDE
3. Tools > Board > Arduino Mega or Mega 2560
4. change your APT value at #54,#56,#57
5. change your sim card apn value at #14,#15,#16
6. Click upload
