// /src/pages.js

import React from 'react';
import SignUp from './SignUp';
import Login from './Login';
import RegisterBike from './RegisterBike';
import ManageBike from './ManageBike';
import RentBike from './RentBike';
import Test from './AppBackUp';
/**
 * These are root pages
 */


const Index = () => {
    return(
        <Login/>
    );
};

const Home = () => {
    return <h1 className="py-3">Wellcome</h1>;
};

const signup = () => {
    return(<SignUp/>);
};

const MyBike = () => {
    return(
        <ManageBike/>
    );
};

const RegisterBike_ = () => {
    return(
        <RegisterBike/>
    ); 
};


const rentBike = () => {
    return(
        <RentBike/>
    );
};

const testing = () => {
    return(
        <Test/>
    );
};

export { Index,Home, MyBike, rentBike,signup,RegisterBike_,testing};