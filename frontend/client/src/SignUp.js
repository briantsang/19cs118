

import React, { Component } from "react";

import "./SignUp.css";

export default class Project extends Component {
    constructor() {
        super()
        this.state = {
            UserName: '',
            UserEmail: '',
            UserPassword: ''
        }
        this.handleName = this.handleName.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        //this.UserRegistrationFunction = this.UserRegistrationFunction.bind(this);
    }

    UserRegistrationFunction = () => {
        fetch('http://192.168.0.117/User_Project/user_registration.php', {    //IMPORTANT change ip
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: this.state.UserName,
                email: this.state.UserEmail,
                password: this.state.UserPassword
            })

        }).then((response) => response.json())
            .then((responseJson) => {

                // Showing response message coming from server after inserting records.
                window.alert(responseJson);

            }).catch((error) => {
                console.error(error);
            });
    }

    handleName(event) {
        this.setState({ UserName: event.target.value });
    }
    handleEmail(event) {
        this.setState({ UserEmail: event.target.value });
    }
    handlePassword(event) {
        this.setState({ UserPassword: event.target.value });
    }


    render() {
        return (
            <div className="login">
            <h1>Sign Up</h1>
            <form>
                <p>
                    <input id="Name" name="Name" type="text" placeholder="Username" value={this.state.UserName} onChange={this.handleName}/>
		        </p>
                <p>
                    <input id="Email" name="Email" type="text" placeholder="Email" value={this.state.UserEmail} onChange={this.handleEmail}/>
		        </p>
                <p>
                    <input id="Password" name="Password" type="password" placeholder="Password" value={this.state.UserPassword} onChange={this.handlePassword}/>
		        </p>
                    <button type="button" onClick={this.UserRegistrationFunction}>Create My Account</button>
            </form>
            </div>
        );
    }
}
