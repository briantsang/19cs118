import React, { Component } from "react";
import SimpleStorageContract from "./contracts/SimpleStorage.json";
import Lock from "./contracts/Lock.json";
import getWeb3 from "./getWeb3";


import "./SignUp.css";
var AppSelf;

export default class RegisterBike extends Component {
    
    constructor(props) {
        super(props);
        this.state = { web3: null, accounts: null, contract: null,defaultaccount: null,
            price:'',deposit:'',api:'',apiKey:'',deviceid:'',lat:'',lon:'' };
        this.handlePrice=this.handlePrice.bind(this);
        this.handleDeposit=this.handleDeposit.bind(this);
        this.handleApi=this.handleApi.bind(this);
        this.handleApikey=this.handleApikey.bind(this);
        this.handleDeviceid=this.handleDeviceid.bind(this);
        this.deployNewLock=this.deployNewLock.bind(this);
        
    }
   
  componentDidMount =  async () => {
    try {
        AppSelf = this;
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      
      const networkId = await web3.eth.net.getId();
      const instance = new web3.eth.Contract(
        Lock.abi,
      );

      
      console.log(
                `Success to load web3, accounts  at RegisterBike`,
           );
      this.setState({ web3, accounts, contract: instance,defaultaccount:accounts[0] });
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`,
      );
      console.error(error);
    }
  };


    deployNewLock = () => {
        const { accounts, contract,price,deposit } = this.state;
    contract.deploy({
      data: Lock.bytecode,
      arguments: [price, deposit]
    }).send({
      from:accounts[0],
      gas: 1500000,
      gasPrice: '30000000000000'
    }, function(error, transactionHash){})
    .on('error', function(error){window.alert('error')})
    .on('transactionHash', function(transactionHash){})
    .on('receipt', function(receipt){
     console.log(receipt.contractAddress) // contains the new contract address
    })
    .on('confirmation', function(confirmationNumber, receipt){})
    .then(function(newContractInstance){
      console.log(newContractInstance.options.address) // instance with the new contract address
      window.alert('deploy new contract at'+newContractInstance.options.address);

      //get gsp from api value
      fetch('http://'+this.state.api+'/devices/'+this.state.deviceID+'/datastreams/location', { 
        method: 'GET',
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
          'api-key': this.state.apiKey
        },
        mode: 'cors'
      }).then((response) => {
        console.log(response);
        return response.json();
      }).then((data) => {
        AppSelf.setState({lat: data.data.current_value.lat,
                          lon: data.data.current_value.lon});

      }).catch((error) => {
        console.error(error);
      });

      
      fetch('http://192.168.0.117/User_Project/bike_reg.php', {    //IMPORTANT change ip
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            api: AppSelf.state.api,
            apiKey: AppSelf.state.apiKey,
            deviceID: AppSelf.state.deviceid,
            owner:accounts[0],
            address:newContractInstance.options.address,
            lat: AppSelf.state.lat,
            lon:AppSelf.state.lon,
            price:AppSelf.state.price,
            deposit:AppSelf.state.deposit
        })
  
    }).then((response) => response.json())
        .then((responseJson) => {
  
            // Showing response message coming from server after inserting records.
            window.alert(responseJson);
  
        }).catch((error) => {
            console.error(error);
        });

    });
  };


    handlePrice(event) {
        this.setState({ price: event.target.value },this.log);
    }
    handleDeposit(event) {
        this.setState({ deposit: event.target.value });
    }
    handleApi(event) {
        this.setState({ api: event.target.value });
    }
    handleApikey(event) {
        this.setState({ apiKey: event.target.value });
    }
    handleDeviceid(event) {
        this.setState({ deviceid: event.target.value });
    }   

    log() {
        console.log(this.state.price);
    }



  render() {
    return (
        <div className="login">
        <h1>Register Bike</h1>
        <form>
            <p>
                <input id="Price" name="Price" type="text" placeholder="Price" width="10" value={this.state.price} onChange={this.handlePrice}/><span>/15 mins</span>
            </p>
            <p>
                <input id="Deposit" name="Deposit" type="text" placeholder="Deposit" value={this.state.deposit} onChange={this.handleDeposit}/>
            </p>
            <p>
                <input id="API" name="API" type="text" placeholder="API Address" value={this.state.api} onChange={this.handleApi} />
            </p>
            <p>
                <input id="APIKEY" name="APIKEY" type="text" placeholder="API KEY" value={this.state.apiKey} onChange={this.handleApikey}/>
            </p>
            <p>
                <input id="DeviceID" name="DeviceID" type="text" placeholder="DeviceID" value={this.state.deviceid} onChange={this.handleDeviceid}/>
            </p>
                <button type="button" onClick={this.deployNewLock}>Deploy Smart Contract</button>
        </form>
        </div>

    );
  }

}


