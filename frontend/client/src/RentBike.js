

import React, { Component } from "react";
import { Link } from 'react-router-dom';
import DataGrid from 'react-data-grid';
import 'react-data-grid/dist/react-data-grid.css';
import Lock from "./contracts/Lock.json";
import getWeb3 from "./getWeb3";
import "./table.css";


import { Map, GoogleApiWrapper , InfoWindow, Marker} from 'google-maps-react';
const mapStyles = {
    width: '70%',
    height: '50%'
};

const hStyle = { color: 'white' };
const hStyle2 = { width: '100%' };

var AppSelf;


export class RentBike extends Component {
  constructor(props) {
    super(props);
    this.state = { web3: null, accounts: null, contract: null,defaultaccount: null,
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
      rent:'',return:'',currentAddress:'',deviceID:[],rentable:true,lat:'',lon:''
    };  
    this.handleRent=this.handleRent.bind(this);
    this.handleReturn=this.handleReturn.bind(this);
    this.rentBike=this.rentBike.bind(this);
    this.returnBike=this.returnBike.bind(this);
    
  }

componentDidMount =  async () => {
  try {
      AppSelf = this;
    // Get network provider and web3 instance.
    const web3 = await getWeb3();

    // Use web3 to get the user's accounts.
    const accounts = await web3.eth.getAccounts();

    // Get the contract instance.
    
    const networkId = await web3.eth.net.getId();
    //const deployedNetwork = SimpleStorageContract.networks[networkId];
    const instance = new web3.eth.Contract(
      Lock.abi,
      //deployedNetwork && deployedNetwork.address,
    );
    // Set web3, accounts, and contract to the state, and then proceed with an
    // example of interacting with the contract's methods.
    
    console.log(
              `Success to load web3, accounts  at RentBike`,
         );
    //this.setState({ web3, accounts, contract: instance,defaultaccount:accounts[0] }, this.runExample);
    this.setState({ web3, accounts, contract: instance,defaultaccount:accounts[0] });


    fetch("http://192.168.0.117/User_Project/getDevice.php")
        .then(response => response.json())
        .then((json) => this.setState({ deviceID: json}));

  } catch (error) {
    // Catch any errors for any of the above operations.
    alert(
      `Failed to load web3, accounts, or contract. Check console for details.`,
    );
    console.error(error);
  }
};

rentBike= async(address_)=>{

   const {web3, defaultaccount, contract } = this.state;
   const instance = new web3.eth.Contract(
       Lock.abi,
       address_);
       //const cash=await instance.methods.getBalance().call();
       //await alert(cash);
       await instance.methods.rent().send({ from: defaultaccount, gas: 1500000 ,value:60});
       await this.setState({currentAddress : address_});
  
}

returnBike= async(address_)=>{

  const {web3, defaultaccount, contract } = this.state;
  const instance = new web3.eth.Contract(
      Lock.abi,
      address_);
 instance.methods.returnBack().send({ from: defaultaccount, gas: 1500000});
 this.setState({currentAddress : ""});

      //get gsp from api value
      fetch('http://'+this.state.api+'/devices/'+this.state.deviceID+'/datastreams/location', { 
        method: 'GET',
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
          'api-key': this.state.apiKey
        },
        mode: 'cors'
      }).then((response) => {
        console.log(response);
        return response.json();
      }).then((data) => {
        AppSelf.setState({lat: data.data.current_value.lat,
                          lon: data.data.current_value.lon});

      }).catch((error) => {
        console.error(error);
      });

      fetch('http://192.168.0.117/User_Project/updateGps.php', {    //IMPORTANT change ip
      method: 'POST',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
          address:address_,
          lat:AppSelf.state.lat,
          lon:AppSelf.state.lon
      })
  
  }).then((response) => response.json())
      .then((responseJson) => {
      }).catch((error) => {
          console.error(error);
      });


}
           


    // state = {
    //     showingInfoWindow: false,
    //     activeMarker: {},
    //     selectedPlace: {},
    //   };
    
      onMarkerClick = (props, marker, e) =>
        this.setState({
          selectedPlace: props,
          activeMarker: marker,
          showingInfoWindow: true
        });
        onMapClicked = (props) => {
            if (this.state.showingInfoWindow) {
              this.setState({
                showingInfoWindow: false,
                activeMarker: null
              })
            }
          };

      
        handleRent(event) {
            this.setState({ rent: event.target.value });
        }
        handleReturn(event) {
            this.setState({ return: event.target.value });
        }
        
         
    
    render() {
        return (
            <div>

<table className="blueTable">
          <thead>
            <tr>
              <th>ID</th>
              <th>Owner</th>
              <th>LAT</th>
              <th>LON</th>
              <th>Contract Address</th>
              <th>Price</th>
              <th>Deposit</th>
            </tr>
          </thead>
          <tbody>
          { this.state.deviceID.map(deviceID => 
          
          <tr>
            <td>{deviceID.id}</td>
            <td>{deviceID.owner}</td>
            <td>{deviceID.lat}</td>
            <td>{deviceID.lon}</td>
            <td>{deviceID.address}</td>
            <td>{deviceID.price}</td>
            <td>{deviceID.deposit}</td>
            </tr>
          )}
          </tbody>
        </table>

            <center><h2 style={hStyle}>Current Contract :</h2> <input style={hStyle2} id="current" type="texr" readOnly value={this.state.currentAddress}></input></center>
            <p>
                
                <input id="Rent" name="Rent" type="text" placeholder="Contract Address" value={this.state.rent} onChange={this.handleRent}/>
                <button type="button" onClick={async () => {await this.rentBike(this.state.rent);} }>Rent</button>
                <br></br>
                <input id="Return" name="Return" type="text" placeholder="Contract Address" value={this.state.return} onChange={this.handleReturn} />
                <button type="button" onClick={async () => {await this.returnBike(this.state.return);} }>Return</button>
                
		    </p>


            <Map google={this.props.google}
           initialCenter={{
             lat: 22.501261,
             lng: 114.13113
           }}
           zoom={15}
           style={mapStyles}
           onClick={this.onMapClicked}>

          { this.state.deviceID.map(deviceID => 
            <Marker onClick={this.onMarkerClick}
            name={'id: '+deviceID.id}
            position={{lat: deviceID.lat, lng: deviceID.lon}}/>
          )}
        

        <InfoWindow
          marker={this.state.activeMarker}
          visible={this.state.showingInfoWindow}>
          <div>
            <h3>{this.state.selectedPlace.name}</h3>
          </div>
        </InfoWindow>
      </Map>

            </div>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyAYarWknUbV11HvtsqPua2aHamNpHxfEQA'
  })(RentBike);
