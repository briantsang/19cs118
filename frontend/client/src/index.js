import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch } from 'react-router-dom';
import './index.css';
import './SignUp.css';
import "./table.css";
import App from './App';
import SignUp from './SignUp';
import * as serviceWorker from './serviceWorker';
import 'react-data-grid/dist/react-data-grid.css';

//ReactDOM.render(<App />, document.getElementById('root'));
//ReactDOM.render(<SignUp />, document.getElementById('root'));
ReactDOM.render(
    <BrowserRouter>
      <Switch>
        <App />
      </Switch>
    </BrowserRouter>,
    document.getElementById('root')
  );




// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
