import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import {Index,Home, MyBike, rentBike,signup,RegisterBike_,testing } from './pages';
import { Navbar } from './components';
import 'react-data-grid/dist/react-data-grid.css';



import getWeb3 from "./getWeb3";
import Lock from "./contracts/Lock.json";

class App extends Component {
  state = {web3: null, accounts: null, contract:null};
  // componentDidMount = async () => {
  //   //connectWeb3 = async () => {
  //     try {
  //       // Get network provider and web3 instance.
  //       const web3 = await getWeb3();
  
  //       // Use web3 to get the user's accounts.
  //       const accounts = await web3.eth.getAccounts();
  
  //       // Get the contract instance.
  //       const networkId = await web3.eth.net.getId();

  //       const Lockcontract=new web3.eth.Contract(Lock.abi);
      
  //       // Set web3, accounts, and contract to the state, and then proceed with an
  //       // example of interacting with the contract's methods.
  //       this.setState({ web3, accounts,contract:Lockcontract});
  //       alert(
  //         `Success to load web3, accounts in App.js`,
  //       );
  //     } catch (error) {
  //       // Catch any errors for any of the above operations.
  //       alert(
  //         `Failed to load web3, accounts, or contract. Check console for details.`,
  //       );
  //       console.error(error);
  //     }
  //   };
  render() {
    return(
      <div className="container">
        {/* The corresponding component will show here if the current URL matches the path */}
        <Navbar/>
        <Route path="/" exact component={Index} />
        <Route path="/Home" exact component={Home} />
        <Route path="/MyBike" component={MyBike} />
        <Route path="/RentBike" component={rentBike} />
        <Route path="/signup" component={signup} />
        <Route path="/registerBike" component={RegisterBike_}/>
        <Route path="/testing" component={testing} />
      </div>
    );
    
  }
}

export default App;



