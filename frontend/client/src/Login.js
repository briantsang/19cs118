

import React, { Component } from "react";
import { Link } from 'react-router-dom';
import "./SignUp.css";

export default class Login extends Component {
    constructor() {
        super()
        this.state = {
            UserId: '',
            UserEmail: '',
            UserPassword: ''
        }
        this.handleEmail = this.handleEmail.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        //this.UserRegistrationFunction = this.UserRegistrationFunction.bind(this);
    }

    UserLoginFunction = () => {
        fetch('http://192.168.0.117/User_Project/User_Login.php', {    //IMPORTANT change ip
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.state.UserEmail,
                password: this.state.UserPassword
            })

        }).then((response) => response.json())
            .then((responseJson) => {

                // Showing response message coming from server after inserting records.
                window.alert(responseJson);
                if(responseJson=="Data Matched"){
                    window.location.href='Home'
                }
                //this.setState({ UserId: responseJson });

            }).catch((error) => {
                console.error(error);
            });
    }


    handleEmail(event) {
        this.setState({ UserEmail: event.target.value });
    }
    handlePassword(event) {
        this.setState({ UserPassword: event.target.value });
    }


    render() {
        return (
            <div className="login">
            <h1>Login</h1>
            <form>
                <p>
                    <input id="Email" name="Email" type="text" placeholder="Email" value={this.state.UserEmail} onChange={this.handleEmail}/>
		        </p>
                <p>
                    <input id="Password" name="Password" type="password" placeholder="Password" value={this.state.UserPassword} onChange={this.handlePassword}/>
		        </p>
                    <button type="button" onClick={this.UserLoginFunction}>Login</button>
                    <Link className="nav-link" to="/signup">Sign up a new account</Link>
            </form>
            </div>
        );
    }
}
