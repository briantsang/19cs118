

import React, { Component } from "react";
import { Link } from 'react-router-dom';
import DataGrid from 'react-data-grid';
import 'react-data-grid/dist/react-data-grid.css';
import Lock from "./contracts/Lock.json";
import getWeb3 from "./getWeb3";
import "./table.css";

import { Map, GoogleApiWrapper, InfoWindow, Marker } from 'google-maps-react';
const mapStyles = {
  width: '70%',
  height: '50%'
};


const tablestyle={color: "#f5f5f5",border: "3px"};

var AppSelf;
var AppSelf1;
var AppSelf2;

class BalanceClass extends React.Component{
  
  constructor(props) {
    
    super(props);
    AppSelf1=this;
    this.state={balance:null};
  }
  start = () =>{
    try {
      
    const web3 = this.props.web3;
      const instance= new web3.eth.Contract(
      Lock.abi,
      this.props.TempAddress);
      instance.methods.getBalance().call(function(err,res){
        AppSelf1.setState({balance:res});
    });
  } catch (error) {
    // Catch any errors for any of the above operations.
    alert(
      `Failed`,
    );
    console.error(error);
  }
  };

       render() {
         {this.start()}
        const balance = this.state.balance;
        return(
            <td key={1}>{balance}</td>
        );
      }

}

class StateClass extends React.Component{

  constructor(props) {
    super(props);
    AppSelf2=this;
    this.state={st:null};
  }
  start = () =>{
    try {
      
    const web3 = this.props.web3;
      const instance= new web3.eth.Contract(
      Lock.abi,
      this.props.TempAddress);
      instance.methods.getState().call(function(err,res){
      AppSelf2.setState({st: res});
    });
  } catch (error) {
    // Catch any errors for any of the above operations.
    alert(
      `Failed`,
    );
    console.error(error);
  }
  };

       render() {
         {this.start()}
        const st = this.state.st;
        return(
            <td key={1}>{st}</td>
        );
      }

}




export class ManageBike extends Component {
  constructor(props) {
    super(props);
    this.state = {
      web3: null, accounts: null, contract: null, defaultaccount: null,
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
      getBalance_: '', unlock: '', setAddress: '', price: '', deposit: '', cashOutAmount: '',
      deviceID: [],dbcaddress:null,
      st:'LOCK',balance:'',lat:'',lon:''
    };
    this.handlegetBalance = this.handlegetBalance.bind(this);
    this.handleunlock = this.handleunlock.bind(this);
    this.handlesetAddress = this.handlesetAddress.bind(this);
    this.handleprice = this.handleprice.bind(this);
    this.handledeposit = this.handledeposit.bind(this);

  }

  componentDidMount = async () => {
    try {
      AppSelf = this;
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.

      const networkId = await web3.eth.net.getId();
      //const deployedNetwork = SimpleStorageContract.networks[networkId];
      const instance = new web3.eth.Contract(
        Lock.abi,
        //deployedNetwork && deployedNetwork.address,
      );
      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.

      console.log(
        `Success to load web3, accounts  at ManageBike`,
      );
      //this.setState({ web3, accounts, contract: instance,defaultaccount:accounts[0] }, this.runExample);
      this.setState({ web3, accounts, contract: instance, defaultaccount: accounts[0] });

      fetch("http://192.168.0.117/User_Project/getDevice.php")
        .then(response => response.json())
        .then((json) => this.setState({ deviceID: json}));
        

    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`,
      );
      console.error(error);
    }
  };

  // state = {
  //     showingInfoWindow: false,
  //     activeMarker: {},
  //     selectedPlace: {},
  //   };

  cashOut = async (address_) => {

    const { web3, defaultaccount, contract } = this.state;
    const instance = new web3.eth.Contract(
      Lock.abi,
      address_);
    const cash = await instance.methods.getBalance().call();
    await instance.methods.cashOut().send({ from: defaultaccount, gas: 1500000 });
    await window.alert('Cash out ' + cash + ' Wei from ' + address_);

    this.setState({balance: 0});

  }

  unlock = async (address_) => {

    const { web3, defaultaccount, contract } = this.state;
    const instance = new web3.eth.Contract(
      Lock.abi,
      address_);
    await instance.methods.ownerOpen().send({ from: defaultaccount, gas: 1500000 });

    await this.setState({st: 'UNLOCK'});

  }

  lock = async (address_) => {

    const { web3, defaultaccount, contract } = this.state;
    const instance = new web3.eth.Contract(
      Lock.abi,
      address_);
    await instance.methods.ownerClose().send({ from: defaultaccount, gas: 1500000 });

    await this.setState({st: 'LOCK'});

  }

  setPrice = async (address_, price_, deposit_) => {

    const { web3, defaultaccount, contract } = this.state;
    const instance = new web3.eth.Contract(
      Lock.abi,
      address_);
    instance.methods.createLock(price_, deposit_).send({ from: defaultaccount, gas: 1500000 });
    

    fetch('http://192.168.0.117/User_Project/updatePrice.php', {    //IMPORTANT change ip
    method: 'POST',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    },
    body: JSON.stringify({
        address:address_,
        price:AppSelf.state.price,
        deposit:AppSelf.state.deposit
    })

}).then((response) => response.json())
    .then((responseJson) => {
    }).catch((error) => {
        console.error(error);
    });

  }

  onMarkerClick = (props, marker, e) =>
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    });
  onMapClicked = (props) => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      })
    }
  };

  handlegetBalance(event) {
    this.setState({ getBalance_: event.target.value });
  }
  handleunlock(event) {
    this.setState({ unlock: event.target.value });
  }
  handlesetAddress(event) {
    this.setState({ setAddress: event.target.value });
  }
  handleprice(event) {
    this.setState({ price: event.target.value });
  }
  handledeposit(event) {
    this.setState({ deposit: event.target.value });
  }



  render() {
    return (
      <div>
        {/* <DataGrid
                columns={columns}
                rows={rows}
            /> */}
        <table className="blueTable">
          <thead>
            <tr>
              <th>ID</th>
              <th>Owner</th>
              <th>LAT</th>
              <th>LON</th>
              <th>Contract Address</th>
              <th>Price</th>
              <th>Deposit</th>
              <th>State</th>
              <th>Balance</th>
              
             
            </tr>
          </thead>
          <tbody>
          { this.state.deviceID.map(deviceID => 
          
          <tr>
            <td>{deviceID.id}</td>
            <td>{deviceID.owner}</td>
            <td>{deviceID.lat}</td>
            <td>{deviceID.lon}</td>
            <td>{deviceID.address}</td>
            <td>{deviceID.price}</td>
            <td>{deviceID.deposit}</td>
            <StateClass 
            web3={this.state.web3}
            TempAddress={deviceID.address}/>
            <BalanceClass 
            web3={this.state.web3}
            TempAddress={deviceID.address}/>
          
            

            </tr>
          )}
          </tbody>
        </table>
        <p>

          <input id="getBalance" name="getBalance" type="text" placeholder="Contract Address" value={this.state.getBalance_} onChange={this.handlegetBalance} />
          <button type="button" onClick={async () => { await this.cashOut(this.state.getBalance_); }}>getBalance</button>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input id="Unlock" name="Unlock" type="text" placeholder="Contract Address" value={this.state.unlock} onChange={this.handleunlock} />
                <button type="button" onClick={async () => { await this.unlock(this.state.unlock); }}>Unlock</button>
                <button type="button" onClick={async () => { await this.lock(this.state.unlock); }}>Lock</button>
          <br></br>
          <input id="setAddress" name="setAddress" type="text" placeholder="Contract Address" value={this.state.setAddress} onChange={this.handlesetAddress} />
          <input id="Price" name="Price" type="text" placeholder="Price" value={this.state.price} onChange={this.handleprice} />
          <input id="Deposit" name="Deposit" type="text" placeholder="Deposit" value={this.state.deposit} onChange={this.handledeposit} />
          <button type="button" onClick={async () => { await this.setPrice(this.state.setAddress, this.state.price, this.state.deposit); }}>Set price and deposit</button>


        </p>
        <Map google={this.props.google}
          initialCenter={{
            lat: 22.501261,
            lng: 114.13113
          }}
          zoom={15}
          style={mapStyles}
          onClick={this.onMapClicked}>
          { this.state.deviceID.map(deviceID => 
            <Marker onClick={this.onMarkerClick}
            name={'id: '+deviceID.id}
            position={{lat: deviceID.lat, lng: deviceID.lon}}/>
          )}

          <InfoWindow
            marker={this.state.activeMarker}
            visible={this.state.showingInfoWindow}>
            <div>
              <h3>{this.state.selectedPlace.name}</h3>
            </div>
          </InfoWindow>
        </Map>

      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyAYarWknUbV11HvtsqPua2aHamNpHxfEQA'
})(ManageBike);
