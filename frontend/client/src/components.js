import React from 'react';
import { Link } from 'react-router-dom';
import bike from './bike.png';

const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-sm navbar-light bg-light">
      <Link className="navbar-brand" to="/">
        <img src={bike} alt="react-router-breadcrumb" width="30" height="30" />
      </Link>

      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarContent"
        aria-controls="navbarContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>

      <div className="collapse navbar-collapse" id="navbarContent">
        <ul className="navbar-nav">
          <li className="nav-item">
            <Link className="nav-link" to="/Home">
              Home
            </Link>
          </li>
          <li className="nav-item">
              <a className="nav-link" target="_self" href="http://localhost:3000/MyBike">MyBike</a>
            
          </li>
          <li className="nav-item">
              <a className="nav-link" target="_self" href="http://localhost:3000/registerBike">RegisterBike</a>
          </li>
          <li className="nav-item">
            <a className="nav-link" target="_self" href="http://localhost:3000/RentBike">RentBike</a>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export { Navbar };