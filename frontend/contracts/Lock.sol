pragma solidity 0.5.1;

contract Lock {
    address payable public Ower;
    address payable public Renter;
    
    event Open();
    event Close();
    event Return(address Renter,uint256 Stime,uint256 Etime,uint money);
    event Rent(address Renter,uint money,uint256 time);
    
    bool public rentable;
    uint public price;//in 15mins
    uint public deposit;//in wei, is the price*30min
    uint256 public startTime;
    uint256 public endTime;
    uint256 public totalPrice;
    
    mapping(address => uint256) balances;
    
    State private state;
    string[2] stateName = ["Open", "Close"];
    enum State {Open, Close}
    
    constructor(uint _price, uint _deposit) public payable{
        Ower = msg.sender;
        rentable=true;
        state=State.Close;
        price=_price;
        deposit=_deposit;
    }
    
    
    function () payable external{}
    
    function createLock(uint _price, uint _deposit) public 
    onlyOwer 
    isRentable 
    inState(State.Close){
        price=_price;
        deposit=_deposit;
    }
    
    function ownerOpen() public 
    onlyOwer 
    isRentable 
    inState(State.Close){
        rentable=false;
        state=State.Open;
        emit Open();
    }
    
    function ownerClose() public 
    onlyOwer 
    inState(State.Open){
        rentable=true;
        state=State.Close;
        emit Close();
    }
    
    function rent() public 
    isRentable 
    inState(State.Close)
    payable
    condition(msg.value >= (deposit*1 wei)){
        Renter=msg.sender;
        balances[msg.sender] += msg.value;
        rentable=false;
        state=State.Open;
        startTime=getTime();
        emit Open();
        emit Rent(msg.sender,msg.value,startTime);
    } 
    
    function returnBack() public
    inState(State.Open)
    onlyRenter
    payable{
        uint256 returnPrice;
        endTime=getTime();
        if(((endTime-startTime)/900)<=1){
            returnPrice = balances[Renter] - price;
        }else if(price *((endTime-startTime)/900)<deposit){
          returnPrice = balances[Renter]-(price*(endTime-startTime)/900);
        }else{
            returnPrice=0;
        }
        Renter.transfer(returnPrice);
        state=State.Close;
        rentable=true;
        emit Close();
        emit Return(Renter,startTime,endTime,returnPrice);
        balances[Renter]=0;
        Renter = address(0);
    }
    
/*    function countTime(uint256 totalTime) public pure returns (uint256){
        uint256 temp = totalTime;
        uint256 counter = 1;
        while(temp>900){ //more than 15 mins
            temp-=900;
            counter+=1;
        }
        return counter;
    }*/
    
    function cashOut() public onlyOwer isRentable payable{
        Ower.transfer(address(this).balance);
    }
    
    function getBalance () public view returns (uint256 balance){
        return address(this).balance;
    }
    
    function getState() public view returns(string memory currentState){
        return stateName[uint(state)];
    }
    
    function getTime() private view returns(uint256){
        return now;
    }
    
    modifier onlyOwer() {
        require(msg.sender == Ower);
        _;
    }
    
    modifier onlyRenter() {
        require(msg.sender == Renter);
        _;
    }
    
    modifier isRentable() {
        require(rentable == true);
        _;
    }
    
    modifier inState(State _state) {
        require(state == _state);
        _;
    }
    
    modifier condition(bool _condition) {
        require(_condition);
        _;
    }
}